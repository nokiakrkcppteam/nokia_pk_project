#include "Application.hpp"
#include "States/NotConnectedState.hpp"
#include <mutex>

namespace ue
{

Application::Application(common::PhoneNumber phoneNumber,
                         common::ILogger &iLogger,
                         IBtsPort &bts,
                         IUserPort &user,
                         ITimerPort &timer)
    : context{iLogger, bts, user, timer},
      logger(iLogger, "[APP] ")
{
    logger.logInfo("Started");
    context.setState<NotConnectedState>();
}

Application::~Application()
{
    logger.logInfo("Stopped");
}

void Application::handleAttachTimeout()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleAttachTimeout();
}

void Application::handleIncomingCallTimeout()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleIncomingCallTimeout();
}

void Application::handleOutgoingCallTimeout()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleOutgoingCallTimeout();
}

void Application::handleUnknownRecipientTimeout()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleUnknownRecipientTimeout();
}

void Application::handleSib(common::BtsId btsId)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleSib(btsId);
}

void Application::handleAttachAccept()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleAttachAccept();
}

void Application::handleAttachReject()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleAttachReject();
}

void Application::handleDisconnected()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleDisconnected();
}

void Application::handleSmsReceive(common::PhoneNumber from, const std::string& text)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleSmsReceive(from, text);
}

void Application::handleSmsSend(common::PhoneNumber to, const std::string& text)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleSmsSend(to, text);
}

void Application::handleUnknownRecipientOfSms(common::PhoneNumber to)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleUnknownRecipientOfSms(to);
}

void Application::handleCallAcceptReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallAcceptReceive(from, decryptor);
}
void Application::handleCallDropReceive(common::PhoneNumber from)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallDropReceive(from);
}
void Application::handleCallRequestSend(common::PhoneNumber to, std::shared_ptr<ICoder> encryptor)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallRequestSend(to, encryptor);
}

void Application::handleCallRequestReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallRequestReceive(from, decryptor);
}

void Application::handleIncomingCallDrop()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleIncomingCallDrop();
}

void Application::handleIncomingCallAccept(std::shared_ptr<ICoder> encryptor)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleIncomingCallAccept(encryptor);
}

void Application::handleUnknownRecipientOfCall(common::PhoneNumber to)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleUnknownRecipientOfCall(to);
}

void Application::handleCancelUnknownRecipientShow()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCancelUnknownRecipientShow();
}

void Application::handleOutgoingCallDrop()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleOutgoingCallDrop();
}

void Application::handleUserEndCall()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleUserEndCall();
}

void Application::handleCallTalkSend(const std::string& text)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallTalkSend(text);
}

void Application::handleTalkingTimeout()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleTalkingTimeout();
}

void Application::handleCallTalkReceive(common::PhoneNumber from, const std::string& text)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleCallTalkReceive(from, text);
}

void Application::handleStateClose()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleStateClose();
}

void Application::handleDialing()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleDialing();
}

void Application::handleViewSms(const Sms& sms)
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleViewSms(sms);
}

void Application::handleViewSmsList()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleViewSmsList();
}

void Application::handleSmsCompose()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleSmsCompose();
}

void Application::handleUEClose()
{
    std::lock_guard<std::mutex> l(mut);
    context.state->handleUEClose();
}

}
