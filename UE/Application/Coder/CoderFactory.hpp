#pragma once

#include <cstdlib>
#include "ICoder.hpp"
#include "XOREncryption.hpp"
#include "CaesarEncryption.hpp"
#include "NoCoder.hpp"
#include "Messages/IncomingMessage.hpp"

namespace ue
{

class CoderFactory
{
public:
    static ICoder* createCoder(EncryptionType type);
    static ICoder* createCoder(common::IncomingMessage &msg);
};

}
