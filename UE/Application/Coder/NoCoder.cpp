#include "NoCoder.hpp"

namespace ue
{
NoCoder::NoCoder()
{}


std::string NoCoder::encodeMessage(std::string text)
{
   return text;
}

std::string NoCoder::decodeMessage(std::string text)
{
    return text;
}

void NoCoder::addEncryptionInfo(common::OutgoingMessage &msg)
{
    msg.writeNumber<std::uint8_t>(static_cast<std::uint8_t>(EncryptionType::None));
}

}
