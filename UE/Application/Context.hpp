#pragma once

#include "IEventsHandler.hpp"
#include "Logger/ILogger.hpp"
#include <memory>
#include <functional>

namespace ue
{

struct Context
{
    common::ILogger& logger;
    IBtsPort& bts;
    IUserPort& user;
    ITimerPort& timer;
    std::unique_ptr<IEventsHandler> state{};

    Context(common::ILogger& logger, IBtsPort& bts, IUserPort& user, ITimerPort& timer)
        : logger{logger}, bts{bts}, user{user}, timer{timer}
    {}

    template <typename State, typename ...Arg>
    void setState(Arg&& ...arg)
    {
        state = std::make_unique<State>(*this, std::forward<Arg>(arg)...);
        prevState = actualState;
        actualState = [&]{setState<State>(std::forward<Arg>(arg)...);};
    }

    void setPrevState()
    {
        prevState();
    }

private:
    std::function<void()> actualState;
    std::function<void()> prevState;
};

}
