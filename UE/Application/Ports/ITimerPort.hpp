#pragma once

#include <chrono>

namespace ue
{

#define FOR_ALL_TIMERS(ACTION)  \
    ACTION(Attach)              \
    ACTION(IncomingCall)        \
    ACTION(OutgoingCall)        \
    ACTION(UnknownRecipient)    \
    ACTION(Talking)

class ITimerEventsHandler
{
public:
    virtual ~ITimerEventsHandler() = default;

#define TIMEOUT_VIRTUAL_METHODS(X)  \
    virtual void handle##X##Timeout() = 0;

    FOR_ALL_TIMERS(TIMEOUT_VIRTUAL_METHODS)

#undef TIMEOUT_VIRTUAL_METHODS
};


class ITimerPort
{
public:
    virtual ~ITimerPort() = default;

#define TIMER_VIRTUAL_START_STOP_METHODS(X) \
    virtual void start##X##Timer() = 0;		\
    virtual void stop##X##Timer() = 0;

    FOR_ALL_TIMERS(TIMER_VIRTUAL_START_STOP_METHODS)

#undef TIMER_VIRTUAL_START_STOP_METHODS

};

}
