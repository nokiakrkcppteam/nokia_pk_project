#include "TimerPort.hpp"

namespace ue
{

TimerPort::TimerPort(common::ILogger &logger)
    : logger(logger, "[TIMER PORT]"), shouldRun(true)
{
}

void TimerPort::start(ITimerEventsHandler &handler)
{
    using namespace std::literals::chrono_literals;
    logger.logDebug("Started");
    this->handler = &handler;
    timers[Attach] = Timer(500ms, std::bind(&ITimerEventsHandler::handleAttachTimeout, this->handler));
    timers[IncomingCall] = Timer(30s, std::bind(&ITimerEventsHandler::handleIncomingCallTimeout, this->handler));
    timers[OutgoingCall] = Timer(60s, std::bind(&ITimerEventsHandler::handleOutgoingCallTimeout, this->handler));
    timers[UnknownRecipient] = Timer(3s, std::bind(&ITimerEventsHandler::handleUnknownRecipientTimeout, this->handler));
    timers[Talking] = Timer(120s, std::bind(&ITimerEventsHandler::handleTalkingTimeout, this->handler));
    timerThread = std::thread(&TimerPort::timerThreadFunc, this);
}

void TimerPort::stop()
{
    logger.logDebug("Stoped");
    {
        std::lock_guard<std::mutex> lk(timerMutex);
        handler = nullptr;
        shouldRun = false;
    }
    timerCV.notify_one();
    timerThread.join();
}

#define TIMER_START_STOP_FUNCTION_DEFINITION(X)				\
    void TimerPort::start##X##Timer()						\
    {														\
        {													\
            std::lock_guard<std::mutex> lk(timerMutex);		\
            timers[X].start();								\
        }													\
        logger.logDebug("start",#X,"Timer");				\
        timerCV.notify_one();                               \
    }														\
    void TimerPort::stop##X##Timer()						\
    {														\
        {													\
            std::lock_guard<std::mutex> lk(timerMutex);		\
            timers[X].stop();								\
        }													\
        logger.logDebug("stop",#X,"Timer");					\
        timerCV.notify_one();                               \
    }

FOR_ALL_TIMERS(TIMER_START_STOP_FUNCTION_DEFINITION)

#undef TIMER_START_STOP_FUNCTION_DEFINITION

void TimerPort::timerThreadFunc()
{
    std::unique_lock<std::mutex> lk(timerMutex);
    while(shouldRun){
        if(!anyRunningTimers()) timerCV.wait(lk);
        else if(timerCV.wait_until(lk, getEarliestTimerEnd()) == std::cv_status::timeout)
        {
            for(auto& t : timers)
                if(t.isRunning() && (t.getTimePoint() <= Timer::Clock::now()))
                {
                    t.runCallback();
                    t.stop();
                }
        }
    }
}

Timer::TimePoint TimerPort::getEarliestTimerEnd(){
    Timer::TimePoint minTimePoint = Timer::TimePoint::max();
    for(auto& t : timers){
        if(t.isRunning() && t.getTimePoint() < minTimePoint) minTimePoint = t.getTimePoint();
    }
    return minTimePoint;
}

bool TimerPort::anyRunningTimers()
{
    for(auto& t : timers){
        if(t.isRunning()) return true;
    }
    return false;
}

}
