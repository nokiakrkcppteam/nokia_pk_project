#include "SmsService.hpp"

namespace ue
{

SmsService::SmsService(common::ILogger& logger)
: logger(logger, "[SmsService]")
{}

void SmsService::markAsRead(Sms& sms)
{
    if(sms.status == Status::notRead)
        sms.status = Status::read;
    else
        logger.logError("Uexpected: markAsRead");
}

void SmsService::markAsFailed(Sms& sms)
{
    if(sms.status == Status::sent)
        sms.status = Status::notReceived;
    else
        logger.logError("Uexpected: markAsFailed");
}

}
