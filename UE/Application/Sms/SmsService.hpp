#pragma once

#include "Sms.hpp"
#include "Logger/PrefixedLogger.hpp"

namespace ue
{
   class SmsService
   {
   public:
       SmsService(common::ILogger& logger);
       void markAsRead(Sms& sms);
       void markAsFailed(Sms& sms);
   private:
       common::PrefixedLogger logger;
   };
}
