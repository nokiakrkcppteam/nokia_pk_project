#pragma once
#include "IEventsHandler.hpp"
#include "Logger/PrefixedLogger.hpp"
#include "Context.hpp"

namespace ue
{

class BaseState : public IEventsHandler
{
public:
    BaseState(Context& context, const std::string& name);
    ~BaseState() override;

    // ITimerEventsHandler interface
    void handleAttachTimeout() override;
    void handleIncomingCallTimeout() override;
    void handleOutgoingCallTimeout() override;
    void handleUnknownRecipientTimeout() override;
    void handleTalkingTimeout() override;

    // IBtsEventsHandler interface
    void handleSib(common::BtsId btsId) override;
    void handleAttachAccept() override;
    void handleAttachReject() override;
    void handleDisconnected() override;
    void handleSmsReceive(common::PhoneNumber from, const std::string& text) override;
    void handleUnknownRecipientOfSms(common::PhoneNumber to) override;
    void handleCallAcceptReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor) override;
    void handleCallRequestReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor) override;
    void handleUnknownRecipientOfCall(common::PhoneNumber to) override;
    void handleCallDropReceive(common::PhoneNumber from) override;
    void handleCallTalkReceive(common::PhoneNumber from, const std::string& text) override;

    //IUserEventsHandler interface
    void handleSmsSend(common::PhoneNumber to, const std::string& text) override;
    void handleCallRequestSend(common::PhoneNumber to, std::shared_ptr<ICoder> encryptor) override;
    void handleIncomingCallDrop() override;
    void handleIncomingCallAccept(std::shared_ptr<ICoder> encryptor) override;
    void handleCancelUnknownRecipientShow() override;
    void handleOutgoingCallDrop() override;
    void handleUserEndCall() override;
    void handleCallTalkSend(const std::string& text) override;
    void handleStateClose() override;
    void handleDialing() override;
    void handleViewSms(const Sms& sms) override;
    void handleViewSmsList() override;
    void handleSmsCompose() override;
    void handleUEClose() override;


protected:
    Context& context;
    common::PrefixedLogger logger;
};

}
