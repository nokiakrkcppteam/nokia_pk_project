#include "ConnectedState.hpp"
#include "NotConnectedState.hpp"
#include "MainMenuState.hpp"
#include "IncomingCallState.hpp"
#include "Coder/ICoder.hpp"
#include <memory>

namespace ue
{

ConnectedState::ConnectedState(Context &context)
    : BaseState(context, "ConnectedState")
{
    context.user.showConnected();
}

ConnectedState::ConnectedState(Context& context, const std::string& name)
    : BaseState(context, name)
{
}

void ConnectedState::handleUnknownRecipientTimeout()
{
    logger.logDebug("handleUnknownRecipientTimeout");
    context.setState<MainMenuState>();
}

void ConnectedState::handleDisconnected()
{
    logger.logDebug("handleDisconnected");
    context.setState<NotConnectedState>();
}

void ConnectedState::handleSmsReceive(common::PhoneNumber from, const std::string& text)
{
    logger.logDebug("handleSmsReceive from: ", from);
    context.user.addReceivedSms(from, text);
    context.user.showSmsNotification();
}

void ConnectedState::handleUnknownRecipientOfSms(common::PhoneNumber to)
{
    logger.logDebug("handleUnknownRecipientOfSms to: ", to);
    context.user.markSmsAsFailed(to);
}

void ConnectedState::handleCallRequestReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor)
{
    logger.logDebug("handleCallRequest from: ", from);
    context.setState<IncomingCallState>(from, decryptor);
}

void ConnectedState::handleUnknownRecipientOfCall(common::PhoneNumber to)
{
    logger.logDebug("handleUnknownRecipientOfCall to: ", to);
    context.timer.startUnknownRecipientTimer();
    context.user.showUnknownRecipientOfCall(to);
}

void ConnectedState::handleUEClose()
{

}

}
