#include "ConnectingState.hpp"
#include "MainMenuState.hpp"
#include "NotConnectedState.hpp"

namespace ue
{

ConnectingState::ConnectingState(Context &context, common::BtsId btsId)
    : BaseState(context, "ConnectingState")
{
    context.bts.sendAttachRequest(btsId);
    context.user.showConnecting();
    context.timer.startAttachTimer();
}

void ConnectingState::handleAttachAccept()
{
    logger.logDebug("handleAttachAccept");
    context.timer.stopAttachTimer();
    context.setState<MainMenuState>();
}

void ConnectingState::handleAttachReject()
{
    logger.logDebug("handleAttachReject");
    context.timer.stopAttachTimer();
    context.setState<NotConnectedState>();
}

void ConnectingState::handleAttachTimeout()
{
    logger.logDebug("handleTimeout");
    context.setState<NotConnectedState>();
}

void ConnectingState::handleDisconnected()
{
    logger.logDebug("handleDisconnected");
    context.timer.stopAttachTimer();
    context.setState<NotConnectedState>();
}

void ConnectingState::handleUEClose()
{
    logger.logDebug("handleUEClose");
    context.timer.stopAttachTimer();
    context.setState<NotConnectedState>();
}

void ConnectingState::handleSib(common::BtsId btsId)
{

}

}
