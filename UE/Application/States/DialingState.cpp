#include "DialingState.hpp"
#include "MainMenuState.hpp"
#include "OutgoingCallState.hpp"

namespace ue
{

DialingState::DialingState(Context &context)
    : ConnectedState(context, "DialingState")
{
    context.user.showDialMode();
}

void DialingState::handleStateClose()
{
    logger.logDebug("handleStateClose");
    context.setState<MainMenuState>();
}

void DialingState::handleCallRequestSend(common::PhoneNumber to, std::shared_ptr<ICoder> encryptor)
{
    logger.logDebug("handleCallRequestSend: ", to);
    context.setState<OutgoingCallState>(to, encryptor);
}

}
