#pragma once

#include "ConnectedState.hpp"

namespace ue
{

class DialingState : public ConnectedState
{
public:
    DialingState(Context& context);

    //IUserEventHandler
    void handleStateClose() override;
    void handleCallRequestSend(common::PhoneNumber to, std::shared_ptr<ICoder> encryptor) override;
};

}
