#pragma once

#include "ConnectedState.hpp"

namespace ue
{

class MainMenuState : public ConnectedState
{
public:
    MainMenuState(Context& context);

    //IUserEventHandler
    void handleDialing() override;
    void handleViewSmsList() override;
    void handleSmsCompose() override;
};

}
