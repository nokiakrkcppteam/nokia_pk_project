#include "SmsComposeState.hpp"
#include "MainMenuState.hpp"

namespace ue
{

SmsComposeState::SmsComposeState(Context &context)
    : ConnectedState(context, "SmsComposeState")
{
    context.user.showSmsComposeMode();
}

void SmsComposeState::handleStateClose()
{
    logger.logDebug("handleStateClose");
    context.setState<MainMenuState>();
}

void SmsComposeState::handleSmsSend(common::PhoneNumber to, const std::string& text)
{
    logger.logDebug("handleSendSms to: ", to);
    context.bts.sendSms(to, text);
    context.setState<MainMenuState>();
}

}
