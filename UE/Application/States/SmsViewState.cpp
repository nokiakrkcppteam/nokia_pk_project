#include "SmsViewState.hpp"
#include "SmsListViewState.hpp"

namespace ue
{

SmsViewState::SmsViewState(Context &context, const Sms& sms)
    : ConnectedState(context, "SmsViewState")
{
    context.user.showSms(sms);
}

void SmsViewState::handleStateClose()
{
    logger.logDebug("handleStateClose");
    context.setState<SmsListViewState>();
}

}
