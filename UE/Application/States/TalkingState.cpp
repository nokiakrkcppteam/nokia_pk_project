#include "TalkingState.hpp"
#include "MainMenuState.hpp"

namespace ue
{

TalkingState::TalkingState(Context &context, common::PhoneNumber partnerNumber, std::shared_ptr<ICoder> decryptor, std::shared_ptr<ICoder> encryptor)
    : ConnectedState(context, "TalkingState"), partnerNumber{partnerNumber}, decryptor{decryptor}, encryptor{encryptor}
{
    context.user.showCallMode(partnerNumber);
    context.timer.startTalkingTimer();
}

void TalkingState::handleStateClose()
{
    logger.logDebug("handleStateClose");
    context.timer.stopTalkingTimer();
    context.bts.dropCall(partnerNumber);
    context.setState<MainMenuState>();
}

void TalkingState::handleUnknownRecipientOfCall(common::PhoneNumber to)
{
    logger.logDebug("handleUnknownRecipientOfCall: ", to);
    context.timer.stopTalkingTimer();
    context.timer.startUnknownRecipientTimer();
    context.user.showUnknownRecipientOfCall(to);
}

void TalkingState::handleUnknownRecipientTimeout()
{
    logger.logDebug("handleUnknownRecipientTimeout");
    context.setState<MainMenuState>();
}

void TalkingState::handleCancelUnknownRecipientShow()
{
    logger.logDebug("handleCancelUnknownRecipientShow");
    context.timer.stopUnknownRecipientTimer();
    context.setState<MainMenuState>();
}

void TalkingState::handleUserEndCall()
{
    logger.logDebug("handleUserEndCall");
    context.bts.dropCall(partnerNumber);
    context.timer.stopTalkingTimer();
    context.setState<MainMenuState>();
}

void TalkingState::handleCallDropReceive(common::PhoneNumber from)
{
    logger.logDebug("handleCallDrop: ", from);
    if(from == partnerNumber)
    {
        context.timer.stopTalkingTimer();
        context.setState<MainMenuState>();
    }
    else logger.logError("Received callDrop from different number!");
}

void TalkingState::handleCallTalkSend(const std::string& text)
{
    logger.logDebug("handleCallTalkSend");
    context.timer.stopTalkingTimer();
    context.timer.startTalkingTimer();
    context.bts.sendCallTalk(partnerNumber, encryptor->encodeMessage(text));
}

void TalkingState::handleCallTalkReceive(common::PhoneNumber from, const std::string& text)
{
    logger.logDebug("handleCallTalkReceive");
    if(from == partnerNumber)
    {
        context.timer.stopTalkingTimer();
        context.timer.startTalkingTimer();
        context.user.showIncomingCallTalk(from, decryptor->decodeMessage(text));
    }
    else logger.logError("Received callTalk from different number!");
}

void TalkingState::handleTalkingTimeout()
{
    logger.logDebug("handleTalkingTimeout");
    context.bts.dropCall(partnerNumber);
    context.setState<MainMenuState>();
}

void TalkingState::handleCallRequestReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor)
{
    logger.logDebug("handleCallRequestReceive");
    if(partnerNumber != from)
        context.bts.dropCall(from);
    else
        context.bts.acceptCall(from, encryptor);
}

void TalkingState::handleUEClose()
{
    logger.logDebug("handleUEClose");
    context.timer.stopTalkingTimer();
    context.bts.dropCall(partnerNumber);
}

}
