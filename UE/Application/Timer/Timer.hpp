#pragma once
#include <functional>
#include <chrono>

class Timer
{
public:
    using Callback = std::function<void(void)>;
    using Clock = std::chrono::system_clock;
    using TimePoint = std::chrono::time_point<Clock>;
    using Duration = std::chrono::milliseconds;

    Timer();
    Timer(Duration dur, Callback clbk);

    void start();
    void stop();
    void runCallback();
    bool isRunning();
    TimePoint getTimePoint();

private:
    bool running;
    Callback clbk;
    Duration dur;
    TimePoint tp;
};

