#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Application.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Mocks/IBtsPortMock.hpp"
#include "Mocks/IUserPortMock.hpp"
#include "Mocks/ITimerPortMock.hpp"
#include "Messages/PhoneNumber.hpp"
#include "Coder/ICoder.hpp"
#include "Coder/CoderFactory.hpp"
#include <memory>

namespace ue
{
using namespace ::testing;

class ApplicationTestSuite : public Test
{
protected:
    const common::PhoneNumber PHONE_NUMBER{112}, PHONE_NUMBER2{113}, PHONE_NUMBER3{114};
    const common::BtsId BTS_ID{13121981ll};
    const std::string TEXT{"message"};
    NiceMock<common::ILoggerMock> loggerMock;
    StrictMock<IBtsPortMock> btsPortMock;
    StrictMock<IUserPortMock> userPortMock;
    StrictMock<ITimerPortMock> timerPortMock;
    std::shared_ptr<ICoder> coder{CoderFactory::createCoder(EncryptionType::None)};

    Expectation expectShowNotConnected = EXPECT_CALL(userPortMock, showNotConnected());
    Application objectUnderTest{PHONE_NUMBER,
                                loggerMock,
                                btsPortMock,
                                userPortMock,
                                timerPortMock};
};

struct ApplicationNotConnectedTestSuite : ApplicationTestSuite
{
    void handleSib()
    {
        EXPECT_CALL(btsPortMock, sendAttachRequest(BTS_ID));
        EXPECT_CALL(userPortMock, showConnecting());
        EXPECT_CALL(timerPortMock, startAttachTimer());
        objectUnderTest.handleSib(BTS_ID);
    }
};

TEST_F(ApplicationNotConnectedTestSuite, shallShowNotConnected)
{
}

TEST_F(ApplicationNotConnectedTestSuite, shallHandleSib)
{
    handleSib();
}

struct ApplicationConnectingTestSuite : ApplicationNotConnectedTestSuite
{
    ApplicationConnectingTestSuite()
    {
        handleSib();
    }
    void handleAttachAccept()
    {
        EXPECT_CALL(userPortMock, showMainMenu());
        EXPECT_CALL(timerPortMock, stopAttachTimer());
        objectUnderTest.handleAttachAccept();
    }

    void handleDisconnected()
    {
        EXPECT_CALL(userPortMock, showNotConnected());
        EXPECT_CALL(timerPortMock, stopAttachTimer());
        objectUnderTest.handleDisconnected();
    }
};

TEST_F(ApplicationConnectingTestSuite, shallHandleAttachAccept)
{
    handleAttachAccept();
}

TEST_F(ApplicationConnectingTestSuite, shallHandleAttachReject)
{
    EXPECT_CALL(userPortMock, showNotConnected());
    EXPECT_CALL(timerPortMock, stopAttachTimer());
    objectUnderTest.handleAttachReject();
}

TEST_F(ApplicationConnectingTestSuite, shallHandleAttachTimeout)
{
    EXPECT_CALL(userPortMock, showNotConnected());
    objectUnderTest.handleAttachTimeout();
}

TEST_F(ApplicationConnectingTestSuite, shallHandleDisconnected)
{
    handleDisconnected();
}

TEST_F(ApplicationConnectingTestSuite, shallHandleSib)
{
    objectUnderTest.handleSib(BTS_ID);
}

TEST_F(ApplicationConnectingTestSuite, shallHandleUeClose)
{
    EXPECT_CALL(userPortMock, showNotConnected());
    EXPECT_CALL(timerPortMock, stopAttachTimer());
    objectUnderTest.handleUEClose();
}

struct ApplicationMainMenuTestSuite : ApplicationConnectingTestSuite
{
    ApplicationMainMenuTestSuite(){
        handleAttachAccept();
    }

    void handleDisconnected()
    {
        EXPECT_CALL(userPortMock, showNotConnected());
        objectUnderTest.handleDisconnected();
    }

    void handleCallAccept()
    {
        EXPECT_CALL(userPortMock, showCallMode(PHONE_NUMBER));
        EXPECT_CALL(timerPortMock, stopOutgoingCallTimer());
        objectUnderTest.handleCallAcceptReceive(PHONE_NUMBER, coder);
    }

    void handleSmsReceive()
    {
        EXPECT_CALL(userPortMock, addReceivedSms(PHONE_NUMBER, TEXT));
        EXPECT_CALL(userPortMock, showSmsNotification());
        objectUnderTest.handleSmsReceive(PHONE_NUMBER, TEXT);
    }

};

TEST_F(ApplicationMainMenuTestSuite, shallHandleDisconnected)
{
    handleDisconnected();
}

TEST_F(ApplicationMainMenuTestSuite, shallHandleSmsReceive)
{
    handleSmsReceive();
}

TEST_F(ApplicationMainMenuTestSuite, shallHandleUnknownRecipientOfCall)
{
    EXPECT_CALL(timerPortMock, startUnknownRecipientTimer());
    EXPECT_CALL(userPortMock, showUnknownRecipientOfCall(_));
    objectUnderTest.handleUnknownRecipientOfCall(PHONE_NUMBER2);
}

TEST_F(ApplicationMainMenuTestSuite, shallhandleUnknownRecipientOfSms)
{
    EXPECT_CALL(userPortMock, markSmsAsFailed(PHONE_NUMBER2));
    objectUnderTest.handleUnknownRecipientOfSms(PHONE_NUMBER2);
}

TEST_F(ApplicationMainMenuTestSuite, shallHandleCallRequest)
{
    EXPECT_CALL(userPortMock, showIncomingCall(PHONE_NUMBER2));
    EXPECT_CALL(timerPortMock, startIncomingCallTimer());
    objectUnderTest.handleCallRequestReceive(PHONE_NUMBER2, coder);
}

TEST_F(ApplicationMainMenuTestSuite, shallHandleUnknownRecipientTimeout)
{
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleUnknownRecipientTimeout();
}

TEST_F(ApplicationMainMenuTestSuite, shallHandleDialing)
{
    EXPECT_CALL(userPortMock, showDialMode());
    objectUnderTest.handleDialing();
}

TEST_F(ApplicationMainMenuTestSuite, shallHandleViewSmsList)
{
    EXPECT_CALL(userPortMock, showSmsList());
    objectUnderTest.handleViewSmsList();
}

TEST_F(ApplicationMainMenuTestSuite, shallHandleSmsCompose)
{
    EXPECT_CALL(userPortMock, showSmsComposeMode());
    objectUnderTest.handleSmsCompose();
}

TEST_F(ApplicationMainMenuTestSuite, shallHandleUeClose)
{
    objectUnderTest.handleUEClose();
}

struct ApplicationSmsComposeTestSuite : ApplicationMainMenuTestSuite
{
    ApplicationSmsComposeTestSuite()
    {
        shallHandleSmsCompose();
        objectUnderTest.handleSmsCompose();
    }

    void shallHandleSmsCompose()
    {
        EXPECT_CALL(userPortMock, showSmsComposeMode());
    }
};

TEST_F(ApplicationSmsComposeTestSuite, shallShowSmsCompose)
{

}

TEST_F(ApplicationSmsComposeTestSuite, shallHandleDisconnected)
{
    handleDisconnected();
}

TEST_F(ApplicationSmsComposeTestSuite, shallHandleSmsReceive)
{
    handleSmsReceive();
}

TEST_F(ApplicationSmsComposeTestSuite, shallHandleSmsSend)
{
    EXPECT_CALL(btsPortMock, sendSms(PHONE_NUMBER2,TEXT));
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleSmsSend(PHONE_NUMBER2, TEXT);
}

TEST_F(ApplicationSmsComposeTestSuite, shallShowMenuOnClose)
{
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleStateClose();
}


TEST_F(ApplicationSmsComposeTestSuite, shallHandleUeClose)
{
    objectUnderTest.handleUEClose();
}

struct ApplicationSmsListViewTestSuite : ApplicationMainMenuTestSuite
{
    ApplicationSmsListViewTestSuite()
    {
        shallHandleSmsList();
        objectUnderTest.handleViewSmsList();
    }

    void shallHandleSmsList()
    {
        EXPECT_CALL(userPortMock, showSmsList());
    }
};

TEST_F(ApplicationSmsListViewTestSuite, shallShowSmsList)
{

}

TEST_F(ApplicationSmsListViewTestSuite, shallHandleDisconnected)
{
    handleDisconnected();
}

TEST_F(ApplicationSmsListViewTestSuite, shallShowMenuOnClose)
{
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleStateClose();
}


TEST_F(ApplicationSmsListViewTestSuite, shallHandleUeClose)
{
    objectUnderTest.handleUEClose();
}

TEST_F(ApplicationSmsListViewTestSuite, shallHandleSmsRecive)
{
    EXPECT_CALL(userPortMock, showSmsList());
    handleSmsReceive();
}

struct ApplicationSmsViewTestSuite : ApplicationSmsListViewTestSuite

{
    ApplicationSmsViewTestSuite()
    {
        shallHandleSms();
        objectUnderTest.handleViewSms(sms);
    }

    void shallHandleSms()
    {
        EXPECT_CALL(userPortMock, showSms(_));
    }

private:
    Sms sms;
};

TEST_F(ApplicationSmsViewTestSuite, shallShowSms)
{

}

TEST_F(ApplicationSmsViewTestSuite, shallHandleDisconnected)
{
    handleDisconnected();
}

TEST_F(ApplicationSmsViewTestSuite, shallHandleSmsReceive)
{
    handleSmsReceive();
}

TEST_F(ApplicationSmsViewTestSuite, shallShowSmsListOnClose)
{
    EXPECT_CALL(userPortMock, showSmsList());
    objectUnderTest.handleStateClose();
}

TEST_F(ApplicationSmsViewTestSuite, shallHandleUeClose)
{
    objectUnderTest.handleUEClose();
}


struct ApplicationDialingTestSuite : ApplicationMainMenuTestSuite
{
    ApplicationDialingTestSuite()
    {
        shallHandleDialing();
        objectUnderTest.handleDialing();
    }

    void shallHandleDialing()
    {
        EXPECT_CALL(userPortMock, showDialMode());
    }
};

TEST_F(ApplicationDialingTestSuite, shallShowDialMode)
{

}

TEST_F(ApplicationDialingTestSuite, shallHandleDisconnected)
{
    handleDisconnected();
}

TEST_F(ApplicationDialingTestSuite, shallHandleSmsReceive)
{
    handleSmsReceive();
}

TEST_F(ApplicationDialingTestSuite, shallShowMenuOnClose)
{
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleStateClose();
}

TEST_F(ApplicationDialingTestSuite, shallHandleUeClose)
{
    objectUnderTest.handleUEClose();
}


struct ApplicationOutgoingCallTestSuite : ApplicationDialingTestSuite
{
    ApplicationOutgoingCallTestSuite()
    {
        shallHandleOutgoingCall();
        objectUnderTest.handleCallRequestSend(PHONE_NUMBER2, coder);
    }

    void shallHandleOutgoingCall()
    {
        EXPECT_CALL(userPortMock, showOutgoingCall(PHONE_NUMBER2));
        EXPECT_CALL(timerPortMock, startOutgoingCallTimer());
        EXPECT_CALL(btsPortMock, sendCallRequest(PHONE_NUMBER2, _));
    }
};

TEST_F(ApplicationOutgoingCallTestSuite, shallShowOutgoingCallMode)
{

}

TEST_F(ApplicationOutgoingCallTestSuite, shallHandleDisconnected)
{
    handleDisconnected();
}

TEST_F(ApplicationOutgoingCallTestSuite, shallHandleSmsReceive)
{
    handleSmsReceive();
}

TEST_F(ApplicationOutgoingCallTestSuite, shallHandleCallAcceptReceive)
{
    EXPECT_CALL(timerPortMock, stopOutgoingCallTimer());
    EXPECT_CALL(userPortMock, showCallMode(PHONE_NUMBER2));
    EXPECT_CALL(timerPortMock, startTalkingTimer());
    objectUnderTest.handleCallAcceptReceive(PHONE_NUMBER2,coder);
}

TEST_F(ApplicationOutgoingCallTestSuite, shallHandleCallRequestReceiveFromDiffrentNumber)
{
    EXPECT_CALL(timerPortMock, stopOutgoingCallTimer());
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    EXPECT_CALL(userPortMock, showIncomingCall(PHONE_NUMBER3));
    EXPECT_CALL(timerPortMock, startIncomingCallTimer());
    objectUnderTest.handleCallRequestReceive(PHONE_NUMBER3, coder);
}

TEST_F(ApplicationOutgoingCallTestSuite, shallHandleCallRequestReceiveFromTheSameNumber)
{
    objectUnderTest.handleCallRequestReceive(PHONE_NUMBER2, coder);
}

TEST_F(ApplicationOutgoingCallTestSuite, shallHandleCallDropReceive)
{
    EXPECT_CALL(timerPortMock, stopOutgoingCallTimer());
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleCallDropReceive(PHONE_NUMBER2);
}

TEST_F(ApplicationOutgoingCallTestSuite, shallHandleOutgoinCallTimeout)
{
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleOutgoingCallTimeout();
}

TEST_F(ApplicationOutgoingCallTestSuite, shallShowMenuOnClose)
{
    EXPECT_CALL(timerPortMock, stopOutgoingCallTimer());
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleStateClose();
}

TEST_F(ApplicationOutgoingCallTestSuite, shallHandleUeClose)
{
    EXPECT_CALL(timerPortMock, stopOutgoingCallTimer());
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    objectUnderTest.handleUEClose();
}

struct ApplicationIncomingCallTestSuite : ApplicationMainMenuTestSuite
{
    ApplicationIncomingCallTestSuite()
    {
        shallHandleIncomingCall();
        objectUnderTest.handleCallRequestReceive(PHONE_NUMBER2, coder);
    }

    void shallHandleIncomingCall()
    {
        EXPECT_CALL(userPortMock, showIncomingCall(PHONE_NUMBER2));
        EXPECT_CALL(timerPortMock, startIncomingCallTimer());
    }
};

TEST_F(ApplicationIncomingCallTestSuite, shallShowIncomingCallMode)
{

}

TEST_F(ApplicationIncomingCallTestSuite, shallHandleDisconnected)
{
    handleDisconnected();
}

TEST_F(ApplicationIncomingCallTestSuite, shallHandleSmsReceive)
{
    handleSmsReceive();
}

TEST_F(ApplicationIncomingCallTestSuite, shallHandleIncomingCallAccept)
{
    EXPECT_CALL(timerPortMock, stopIncomingCallTimer());
    EXPECT_CALL(btsPortMock, acceptCall(PHONE_NUMBER2,_));
    EXPECT_CALL(userPortMock, showCallMode(PHONE_NUMBER2));
    EXPECT_CALL(timerPortMock, startTalkingTimer());
    objectUnderTest.handleIncomingCallAccept(coder);
}

TEST_F(ApplicationIncomingCallTestSuite, shallHandleCallDropReceive)
{
    EXPECT_CALL(timerPortMock, stopIncomingCallTimer());
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleCallDropReceive(PHONE_NUMBER2);
}

TEST_F(ApplicationIncomingCallTestSuite, shallHandleIncomingCallTimeout)
{
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleIncomingCallTimeout();
}

TEST_F(ApplicationIncomingCallTestSuite, shallShowPrevStateOnClose)
{
    EXPECT_CALL(timerPortMock, stopIncomingCallTimer());
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleStateClose();
}

TEST_F(ApplicationIncomingCallTestSuite, shallHandleUeClose)
{
    EXPECT_CALL(timerPortMock, stopIncomingCallTimer());
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    objectUnderTest.handleUEClose();
}

TEST_F(ApplicationIncomingCallTestSuite, shallHandleSubsequentCallRequestReceiveFromDifferentNumber)
{
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER3));
    objectUnderTest.handleCallRequestReceive(PHONE_NUMBER3,coder);
}

TEST_F(ApplicationIncomingCallTestSuite, shallHandleSubsequentCallRequestReceiveFromTheSameNumber)
{
    objectUnderTest.handleCallRequestReceive(PHONE_NUMBER2,coder);
}

struct ApplicationTalkingTestSuite : ApplicationOutgoingCallTestSuite
{
    ApplicationTalkingTestSuite()
    {
        shallHandleCallAcceptReceive();
        objectUnderTest.handleCallAcceptReceive(PHONE_NUMBER2,coder);
    }

    void shallHandleCallAcceptReceive()
    {
        EXPECT_CALL(timerPortMock, stopOutgoingCallTimer());
        EXPECT_CALL(userPortMock, showCallMode(PHONE_NUMBER2));
        EXPECT_CALL(timerPortMock, startTalkingTimer());
    }
};

TEST_F(ApplicationTalkingTestSuite, shallShowCallMode)
{

}

TEST_F(ApplicationTalkingTestSuite, shallHandleDisconnected)
{
    handleDisconnected();
}

TEST_F(ApplicationTalkingTestSuite, shallHandleSmsReceive)
{
    handleSmsReceive();
}

TEST_F(ApplicationTalkingTestSuite, shallHandleUnknownRecipientOfCall)
{
    EXPECT_CALL(timerPortMock, startUnknownRecipientTimer());
    EXPECT_CALL(timerPortMock, stopTalkingTimer());
    EXPECT_CALL(userPortMock, showUnknownRecipientOfCall(_));
    objectUnderTest.handleUnknownRecipientOfCall(PHONE_NUMBER2);
}

TEST_F(ApplicationTalkingTestSuite, shallHandleUnknownRecipientTimeout)
{
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleUnknownRecipientTimeout();
}

TEST_F(ApplicationTalkingTestSuite, shallHandleCancelUnknownRepcipientShow)
{
    EXPECT_CALL(timerPortMock, stopUnknownRecipientTimer());
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleCancelUnknownRecipientShow();
}

TEST_F(ApplicationTalkingTestSuite, shallHandleUserEndCall)
{
    EXPECT_CALL(timerPortMock, stopTalkingTimer());
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleUserEndCall();
}

TEST_F(ApplicationTalkingTestSuite, shallHandleCallDrop)
{
    EXPECT_CALL(timerPortMock, stopTalkingTimer());
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleCallDropReceive(PHONE_NUMBER2);
}

TEST_F(ApplicationTalkingTestSuite, shallHandleCallTalkSend)
{
    EXPECT_CALL(timerPortMock, stopTalkingTimer());
    EXPECT_CALL(timerPortMock, startTalkingTimer());
    EXPECT_CALL(btsPortMock, sendCallTalk(PHONE_NUMBER2, TEXT));
    objectUnderTest.handleCallTalkSend(TEXT);
}

TEST_F(ApplicationTalkingTestSuite, shallHandleCallTalkReceive)
{
    EXPECT_CALL(timerPortMock, stopTalkingTimer());
    EXPECT_CALL(timerPortMock, startTalkingTimer());
    EXPECT_CALL(userPortMock, showIncomingCallTalk(PHONE_NUMBER2, TEXT));
    objectUnderTest.handleCallTalkReceive(PHONE_NUMBER2, TEXT);
}

TEST_F(ApplicationTalkingTestSuite, shallHandleTalkingTimeout)
{
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    EXPECT_CALL(userPortMock, showMainMenu());
    objectUnderTest.handleTalkingTimeout();
}

TEST_F(ApplicationTalkingTestSuite, shallHandleUeClose)
{
    EXPECT_CALL(timerPortMock, stopTalkingTimer());
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER2));
    objectUnderTest.handleUEClose();
}

TEST_F(ApplicationTalkingTestSuite, shallHandleCallRequestReceiveFromDifferentNumber)
{
    EXPECT_CALL(btsPortMock, dropCall(PHONE_NUMBER3));
    objectUnderTest.handleCallRequestReceive(PHONE_NUMBER3,coder);
}

TEST_F(ApplicationTalkingTestSuite, shallHandleCallRequestReceiveFromTheSameNumber)
{
    EXPECT_CALL(btsPortMock, acceptCall(PHONE_NUMBER2,_));
    objectUnderTest.handleCallRequestReceive(PHONE_NUMBER2,coder);
}




}
