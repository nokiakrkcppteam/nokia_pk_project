#pragma once

#include <gmock/gmock.h>
#include "Sms/ISmsDb.hpp"
#include "Messages/PhoneNumber.hpp"
#include <vector>

namespace ue
{
    class ISmsDbMock : public ISmsDb
    {
    public:
        ISmsDbMock();
        ~ISmsDbMock() override;

        MOCK_METHOD3(addReceivedSms, void(PhoneNumber, PhoneNumber, const std::string&));
        MOCK_METHOD3(addSentSms, void(PhoneNumber, PhoneNumber, const std::string&));
        MOCK_METHOD0(getReceivedMessages, std::vector<Sms>&());
        MOCK_METHOD0(getSentMessages, std::vector<Sms>&());

    };

}
