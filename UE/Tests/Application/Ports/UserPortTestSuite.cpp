#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Ports/UserPort.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Mocks/IUserPortMock.hpp"
#include "Messages/PhoneNumber.hpp"
#include "Mocks/IUeGuiMock.hpp"
#include "Mocks/ISmsDbMock.hpp"
#include <vector>

namespace ue
{
using namespace ::testing;

class UserPortTestSuite : public Test
{
protected:
    const common::PhoneNumber PHONE_NUMBER{112}, PHONE_NUMBER2{113};
    const std::string TEXT{"message"};
    NiceMock<common::ILoggerMock> loggerMock;
    StrictMock<ICallModeMock> callModeMock;
    StrictMock<IUserEventsHandlerMock> handlerMock;
    StrictMock<IUeGuiMock> guiMock;
    StrictMock<IListViewModeMock> listViewModeMock;
    StrictMock<ISmsComposeModeMock> smsComposeModeMock;
    StrictMock<IDialModeMock> dialModeMock;
    StrictMock<ITextModeMock> textModeMock;
    StrictMock<ISmsDbMock> smsDbMock;

    IUeGui::Callback acceptCallback;
    IUeGui::Callback rejectCallback;
    IUeGui::CloseGuard closeGuard;

    UserPort objectUnderTest{loggerMock, guiMock, PHONE_NUMBER, smsDbMock};

    UserPortTestSuite()
    {
        EXPECT_CALL(guiMock, setTitle(HasSubstr(to_string(PHONE_NUMBER))));
        EXPECT_CALL(guiMock, setCloseGuard(_)).WillOnce(SaveArg<0>(&closeGuard));
        objectUnderTest.start(handlerMock);
    }
    ~UserPortTestSuite()
    {
        objectUnderTest.stop();
    }
};

TEST_F(UserPortTestSuite, shallStartStop)
{
}

TEST_F(UserPortTestSuite, shallShowNotConnected)
{
    EXPECT_CALL(guiMock, showNotConnected());
    objectUnderTest.showNotConnected();
}

TEST_F(UserPortTestSuite, shallShowConnecting)
{
    EXPECT_CALL(guiMock, showConnecting());
    objectUnderTest.showConnecting();
}

TEST_F(UserPortTestSuite, shallMarkSmsAsFailed)
{
    std::vector<Sms> sentMessages;
    sentMessages.emplace_back(Sms{PHONE_NUMBER, PHONE_NUMBER2, TEXT, Status::sent});
    EXPECT_CALL(smsDbMock, getSentMessages()).WillOnce(ReturnRef(sentMessages));
    objectUnderTest.markSmsAsFailed(PHONE_NUMBER2);
    EXPECT_EQ(sentMessages[0].status, Status::notReceived);
}


TEST_F(UserPortTestSuite, shallShowSmsNotification)
{
    EXPECT_CALL(guiMock, showNewSms());
    objectUnderTest.showSmsNotification();
}

TEST_F(UserPortTestSuite, shallAddReceivedSms)
{
    EXPECT_CALL(smsDbMock, addReceivedSms(PHONE_NUMBER2, PHONE_NUMBER, TEXT));
    objectUnderTest.addReceivedSms(PHONE_NUMBER2, TEXT);
}

TEST_F(UserPortTestSuite, shallShowIncomingCall)
{
    EXPECT_CALL(guiMock, setAlertMode()).WillOnce(ReturnRef(textModeMock));
    EXPECT_CALL(textModeMock, setText(HasSubstr(common::to_string(PHONE_NUMBER2))));
    EXPECT_CALL(guiMock, setAcceptCallback(_));
    EXPECT_CALL(guiMock, setRejectCallback(_));
    objectUnderTest.showIncomingCall(PHONE_NUMBER2);
}

struct UserPortMenuTestSuite : UserPortTestSuite
{
    UserPortMenuTestSuite()
    {
        shallShowMenu();
        objectUnderTest.showConnected();
    }

    void shallShowMenu(){
        EXPECT_CALL(guiMock, setListViewMode()).WillOnce(ReturnRef(listViewModeMock));
        EXPECT_CALL(listViewModeMock, clearSelectionList());
        EXPECT_CALL(listViewModeMock, addSelectionListItem(_, _)).Times(AtLeast(1));
        EXPECT_CALL(guiMock, setAcceptCallback(_)).WillOnce(SaveArg<0>(&acceptCallback));
        EXPECT_CALL(guiMock, setRejectCallback(_)).WillOnce(SaveArg<0>(&rejectCallback));
    }

};

TEST_F(UserPortMenuTestSuite, shallShowMenuOnConnected)
{

}

TEST_F(UserPortMenuTestSuite, shallCheckItemIndexOnAccept)
{
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex());
    acceptCallback();
}

struct UserPortSmsComposeModeTestSuite : UserPortMenuTestSuite
{
    UserPortSmsComposeModeTestSuite()
    {
        shallShowSmsComposeMode();
        objectUnderTest.showSmsComposeMode();
    }

    void shallShowSmsComposeMode(){
        EXPECT_CALL(guiMock, setSmsComposeMode()).WillOnce(ReturnRef(smsComposeModeMock));
        EXPECT_CALL(smsComposeModeMock, clearSmsText());
        EXPECT_CALL(smsComposeModeMock, clearPhoneNumber());
        EXPECT_CALL(guiMock, setAcceptCallback(_)).WillOnce(SaveArg<0>(&acceptCallback));
        EXPECT_CALL(guiMock, setRejectCallback(_)).WillOnce(SaveArg<0>(&rejectCallback));
    }
};

TEST_F(UserPortSmsComposeModeTestSuite, shallShowSmsComposeMode)
{

}

TEST_F(UserPortSmsComposeModeTestSuite, shallSendSmsOnAccept)
{
    EXPECT_CALL(smsComposeModeMock, getPhoneNumber());
    EXPECT_CALL(smsComposeModeMock, getSmsText());
    EXPECT_CALL(smsDbMock, addSentSms(_,_,_));
    EXPECT_CALL(handlerMock, handleSmsSend(_,_));
    EXPECT_CALL(smsComposeModeMock, clearSmsText());
    EXPECT_CALL(smsComposeModeMock, clearPhoneNumber());
    acceptCallback();
}

TEST_F(UserPortSmsComposeModeTestSuite, shallHandleStateCloseOnReject)
{
    EXPECT_CALL(smsComposeModeMock, clearSmsText());
    EXPECT_CALL(smsComposeModeMock, clearPhoneNumber());
    EXPECT_CALL(handlerMock, handleStateClose());
    rejectCallback();
}

struct UserPortDialModeTestSuite : UserPortMenuTestSuite
{
    UserPortDialModeTestSuite()
    {
        shallShowDialMode();
        objectUnderTest.showDialMode();
    }

    void shallShowDialMode()
    {
        EXPECT_CALL(guiMock, setDialMode()).WillOnce(ReturnRef(dialModeMock));
        EXPECT_CALL(dialModeMock, clearIncomingText());
        EXPECT_CALL(guiMock, setAcceptCallback(_)).WillOnce(SaveArg<0>(&acceptCallback));
        EXPECT_CALL(guiMock, setRejectCallback(_)).WillOnce(SaveArg<0>(&rejectCallback));
    }

    void shallShowOutgoingCall(common::PhoneNumber phoneNumber)
    {
        EXPECT_CALL(guiMock, setAlertMode()).WillOnce(ReturnRef(textModeMock));
        EXPECT_CALL(textModeMock, setText(HasSubstr(common::to_string(phoneNumber))));
        EXPECT_CALL(guiMock, setAcceptCallback(_));
        EXPECT_CALL(guiMock, setRejectCallback(_));
    }
};

TEST_F(UserPortDialModeTestSuite, shallShowDialMode)
{

}

TEST_F(UserPortDialModeTestSuite, shallShowOutgoingCall)
{
    shallShowOutgoingCall(PHONE_NUMBER2);
    objectUnderTest.showOutgoingCall(PHONE_NUMBER2);
}

TEST_F(UserPortDialModeTestSuite, shallHandleCallRequestSendOnAccept)
{
    EXPECT_CALL(dialModeMock, getPhoneNumber()).WillOnce(Return(PHONE_NUMBER2));
    EXPECT_CALL(dialModeMock, clearPhoneNumber());
    EXPECT_CALL(handlerMock, handleCallRequestSend(PHONE_NUMBER2,_));
    acceptCallback();
}

TEST_F(UserPortDialModeTestSuite, shallHandleStateCloseOnReject)
{
    EXPECT_CALL(dialModeMock, clearPhoneNumber());
    EXPECT_CALL(handlerMock, handleStateClose());
    rejectCallback();
}

struct UserPortCallModeTestSuite : UserPortTestSuite
{
    UserPortCallModeTestSuite()
    {
        shallShowCallMode();
        objectUnderTest.showCallMode(PHONE_NUMBER2);
    }

    void shallShowCallMode()
    {
        EXPECT_CALL(guiMock, setCallMode()).WillOnce(ReturnRef(callModeMock));
        EXPECT_CALL(callModeMock, clearIncomingText());
        EXPECT_CALL(guiMock, setAcceptCallback(_)).WillOnce(SaveArg<0>(&acceptCallback));
        EXPECT_CALL(guiMock, setRejectCallback(_)).WillOnce(SaveArg<0>(&rejectCallback));
    }
};

TEST_F(UserPortCallModeTestSuite, shallShowCallMode)
{
}

TEST_F(UserPortCallModeTestSuite, shallShowIncomingCallTalk)
{
    EXPECT_CALL(guiMock, setCallMode()).WillOnce(ReturnRef(callModeMock));
    EXPECT_CALL(callModeMock, appendIncomingText(HasSubstr(TEXT)));
    objectUnderTest.showIncomingCallTalk(PHONE_NUMBER2, TEXT);
}

struct UserPortSmsListTestSuite : UserPortMenuTestSuite
{
    UserPortSmsListTestSuite()
    {
        std::vector<Sms> smsList;
        smsList.push_back(Sms{{111},PHONE_NUMBER,"Msg1", Status::notRead});
        smsList.push_back(Sms{{111},PHONE_NUMBER,"Msg2", Status::notRead});
        smsList.push_back(Sms{{111},PHONE_NUMBER,"Msg3", Status::notRead});
        shallShowSmsList(smsList);
        objectUnderTest.showSmsList();
    }

    void shallShowSmsList(std::vector<Sms>& smsList){
        EXPECT_CALL(guiMock, setListViewMode()).WillOnce(ReturnRef(listViewModeMock));
        EXPECT_CALL(listViewModeMock, clearSelectionList());
        EXPECT_CALL(smsDbMock, getReceivedMessages()).WillOnce(ReturnRef(smsList));
        EXPECT_CALL(listViewModeMock, addSelectionListItem(_,_)).Times(int(smsList.size()));
        EXPECT_CALL(guiMock, setAcceptCallback(_)).WillOnce(SaveArg<0>(&acceptCallback));
        EXPECT_CALL(guiMock, setRejectCallback(_)).WillOnce(SaveArg<0>(&rejectCallback));
    }
};

TEST_F(UserPortSmsListTestSuite, shallShowSmsList)
{

}

TEST_F(UserPortSmsListTestSuite, shallHandleStateCloseOnReject)
{
    EXPECT_CALL(handlerMock, handleStateClose());
    rejectCallback();
}

TEST_F(UserPortSmsListTestSuite, shallMarkSmsAsReadAndHideNotification)
{
    std::vector<Sms> smsList;
    smsList.push_back({PHONE_NUMBER2, PHONE_NUMBER, TEXT, Status::notRead});
    EXPECT_CALL(listViewModeMock, getCurrentItemIndex()).WillOnce(Return(std::pair(true, 0)));
    EXPECT_CALL(smsDbMock, getReceivedMessages()).WillOnce(ReturnRef(smsList));
    EXPECT_CALL(guiMock, hideNewSms());
    EXPECT_CALL(handlerMock, handleViewSms(Truly([&](const Sms& sms){return sms.from == PHONE_NUMBER2 && sms.to == PHONE_NUMBER && sms.text == TEXT;})));
    acceptCallback();
    EXPECT_EQ(smsList[0].status, Status::read);
}

struct UserPortSmsTestSuite : UserPortSmsListTestSuite
{
    UserPortSmsTestSuite()
    {
        shallShowSms();
        objectUnderTest.showSms(Sms{{111},PHONE_NUMBER,"Msg",Status::notRead});
    }

    void shallShowSms(){
        EXPECT_CALL(guiMock, setViewTextMode()).WillOnce(ReturnRef(textModeMock));
        EXPECT_CALL(textModeMock, setText(_));
        EXPECT_CALL(guiMock, setAcceptCallback(_)).WillOnce(SaveArg<0>(&acceptCallback));
        EXPECT_CALL(guiMock, setRejectCallback(_)).WillOnce(SaveArg<0>(&rejectCallback));
    }
};

TEST_F(UserPortSmsTestSuite, shallShowSms)
{

}

TEST_F(UserPortSmsTestSuite, shallHanleStateCloseOnReject)
{
    EXPECT_CALL(handlerMock, handleStateClose());
    rejectCallback();
}



}
