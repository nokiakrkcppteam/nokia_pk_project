#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Sms/SmsDb.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Messages/PhoneNumber.hpp"

namespace ue
{
using namespace ::testing;

class SmsDbTestSuite : public Test
{
public:
    const common::PhoneNumber PHONE_NUMBER1{112};
    const common::PhoneNumber PHONE_NUMBER2{113};
    const std::string MESSAGE{"message"};
    NiceMock<common::ILoggerMock> loggerMock;

    SmsDb objectUnderTest{loggerMock};

    SmsDbTestSuite(){}

    ~SmsDbTestSuite(){}
};

TEST_F(SmsDbTestSuite, shallAddSentSms)
{
    unsigned long long initialSize = objectUnderTest.getSentMessages().size();
    objectUnderTest.addSentSms(PHONE_NUMBER1, PHONE_NUMBER2, MESSAGE);

    EXPECT_EQ(objectUnderTest.getSentMessages().size(), ++initialSize);
    EXPECT_EQ(objectUnderTest.getSentMessages().back().status, Status::sent);
}

TEST_F(SmsDbTestSuite, shallAddReceivedSms)
{
    unsigned long long initialSize = objectUnderTest.getReceivedMessages().size();
    objectUnderTest.addReceivedSms(PHONE_NUMBER1, PHONE_NUMBER2, MESSAGE);

    EXPECT_EQ(objectUnderTest.getReceivedMessages().size(), ++initialSize);
    EXPECT_EQ(objectUnderTest.getReceivedMessages().back().status, Status::notRead);
}

}
