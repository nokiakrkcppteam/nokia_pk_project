#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Sms/Sms.hpp"
#include "Sms/SmsService.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Messages/PhoneNumber.hpp"

namespace ue
{
using namespace ::testing;

class SmsServiceTestSuite : public Test
{
public:
    const common::PhoneNumber PHONE_NUMBER1{112};
    const common::PhoneNumber PHONE_NUMBER2{113};
    const std::string MESSAGE{"message"};

    Sms receivedSms{PHONE_NUMBER1, PHONE_NUMBER2, MESSAGE, Status::notRead};
    Sms sentSms{PHONE_NUMBER1, PHONE_NUMBER2, MESSAGE, Status::sent};

    NiceMock<common::ILoggerMock> loggerMock;

    SmsService objectUnderTest{loggerMock};

    SmsServiceTestSuite(){}

    ~SmsServiceTestSuite(){}
};

TEST_F(SmsServiceTestSuite, shallMarkAsRead)
{
    objectUnderTest.markAsRead(receivedSms);
    objectUnderTest.markAsRead(sentSms);

    EXPECT_EQ(receivedSms.status, Status::read);
    EXPECT_EQ(sentSms.status, Status::sent);
}

TEST_F(SmsServiceTestSuite, shallMarkAsFailed)
{
    objectUnderTest.markAsFailed(receivedSms);
    objectUnderTest.markAsFailed(sentSms);

    EXPECT_EQ(receivedSms.status, Status::notRead);
    EXPECT_EQ(sentSms.status, Status::notReceived);
}


}
